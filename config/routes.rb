Rails.application.routes.draw do
  get "/" => "urls#new"
  get '/:id' => "urls#show"
  resources :urls
end
