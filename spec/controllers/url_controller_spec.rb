require 'spec_helper'
require 'rails_helper'
require 'rspec/mocks'

describe UrlsController do
  let(:valid_attributes) {
    {:long_url => "http://example.com"}
  }

  let(:valid_session) { {} }

  describe "GET #show" do
    it "assigns the requested url as @url" do
      url = Url.custom_create valid_attributes[:long_url]
      get :show, {:id => url.to_param}, valid_session
      expect(assigns(:url)).to eq(url)
    end
    it "assigns the requested url as @url when the short url is given as the id" do
      url = Url.custom_create valid_attributes[:long_url]
      get :show, {:id => url.short_url}, valid_session
      expect(assigns(:url)).to eq(url)
    end
  end

  describe "GET #new" do
    it "assigns a new url as @url" do
      get :new, {}, valid_session
      expect(assigns(:url)).to be_a_new(Url)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Url" do
        expect {
          post :create, {:url => valid_attributes}, valid_session
        }.to change(Url, :count).by(1)
      end

      it "assigns a newly created url as @url" do
        post :create, {:url => valid_attributes}
        expect(assigns(:url)).to be_a(Url)
        expect(assigns(:url)).to be_persisted
      end

      it "redirects to the created url" do
        post :create, {:url => valid_attributes}
        expect(response).to redirect_to(Url.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved url as @url" do
        post :create, {:url => { :long_url => "invalid value" }}
        expect(assigns(:url)).to be_a_new(Url)
      end

      it "re-renders the 'new' template" do
        post :create, {:url => { :long_url => "invalid value" }}
        expect(response).to render_template("new")
      end
    end
  end
end
