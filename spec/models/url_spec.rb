require 'rails_helper'
require 'spec_helper'

describe Url do
  it "should create a short url for a given long url" do
    user_supplied_url = "http://decisiv.com/about/careers/"
    url = Url.custom_create(user_supplied_url)
    expect(url.short_url.class).to eq(String)
    expect(url.short_url).not_to be_empty
    expect(url).to be_valid
  end

  it "should raise an error if the given long url is not a an http or https url" do
    bad_url =  "a bad url"
    expect{Url.custom_create(bad_url)}.to raise_error(URI::InvalidURIError)
    bad_url =  "www.wedontsupportwwwasthebeginning.com"
    expect{Url.custom_create(bad_url)}.to raise_error(URI::InvalidURIError)
    bad_url =  "it-has-to-start-with-http-or-https.com"
    expect{Url.custom_create(bad_url)}.to raise_error(URI::InvalidURIError)
  end

  it "should accept an http or https url" do
    user_supplied_url = "http://decisiv.com/about/careers/"
    url = Url.custom_create(user_supplied_url)
    expect(url).to be_valid

    user_supplied_url = "https://decisiv.com/about/careers/"
    url = Url.custom_create(user_supplied_url)
    expect(url).to be_valid
  end

  describe "custom finder" do
    it "should find the url given the id" do
      user_supplied_url = "http://decisiv.com/about/careers/"
      url = Url.custom_create(user_supplied_url)
      expect(Url.custom_find_by_id("1")).to eq(url)
    end
    it "should find the url given the shorturl" do
      url = Url.create(:long_url => "http://example.com", :short_url => "acdefgh")
      expect(Url.custom_find_by_id("acdefgh")).to eq(url)
    end
    it "should handle transcription ambiguities and find the url based on the incorrect short url" do
      url = Url.create(:long_url => "http://example.com", :short_url => "8150VVV")
      expect(Url.custom_find_by_id("BISOUuv")).to eq(url)
    end
  end

end
