require 'spec_helper'
require 'rails_helper'

describe ShortUrlValidator do
  describe "invalid?" do
    it "should return false for a valid short url" do
      expect(ShortUrlValidator.invalid?("acdefgh")).to eq(false)
    end
    it "should return true for a short url that already exists" do
      url = Url.create(:long_url => "http://example.com", :short_url => "acdefgh")
      expect(ShortUrlValidator.invalid?("acdefgh")).to eq(true)
    end
    it "should return true if the short url differs from an existing url by only 1 character" do
      url = Url.create(:long_url => "http://example.com", :short_url => "acdefgh")
      expect(ShortUrlValidator.invalid?("acdefgi")).to eq(true)
    end
    it "should return true if the short url contains an offensive word" do
      expect(ShortUrlValidator.invalid?("f001234")).to eq(true)
      expect(ShortUrlValidator.invalid?("bar1234")).to eq(true)
    end
    it "should return true if the short url is all numbers" do
      expect(ShortUrlValidator.invalid?("0123456")).to eq(true)
    end
  end
end
