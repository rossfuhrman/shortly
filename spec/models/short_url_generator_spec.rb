require 'spec_helper'
require 'rails_helper'
require 'rspec/mocks'

describe ShortUrlGenerator do
  it "should generate a 7 character url" do
    short_url = ShortUrlGenerator.generate
    expect(short_url.class).to eq(String)
    expect(short_url).not_to be_empty
    expect(short_url.length).to eq(7)
  end

  it "should not duplicate short urls" do
    url = Url.create(:long_url => "http://example.com", :short_url => "acdefgh")
    allow(ShortUrlGenerator).to receive(:get_a_short_url).and_return("acdefgh", "acd4567")
    first_short_url = ShortUrlGenerator.generate
    expect(first_short_url).not_to eq(url.short_url)
    expect(first_short_url).to eq("acd4567")
  end

end
