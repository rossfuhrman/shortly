class UrlsController < ActionController::Base

  def show
    id = params[:id]
    @url = Url.custom_find_by_id id
  end

  def new
    @url = Url.new
  end

  def create
    begin
      @url = Url.custom_create(params[:url][:long_url])
      redirect_to @url, notice: 'Url was successfully created.' 
    rescue URI::InvalidURIError
      @url = Url.new
      @url.errors.add(:url, "Url is invalid, please ensure it follows the format of http://example.com")
      render :new
    rescue
      @url = Url.new
      @url.errors.add(:url, "There was an error shortening the url. Please ensure it follows the format of http://example.com and try again")
      render :new
    end
  end

end
