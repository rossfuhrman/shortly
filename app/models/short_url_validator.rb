class ShortUrlValidator
  def self.invalid? short_url
    return true if record_already_exists?(short_url)
    return true if short_url_is_too_similar_to_an_existing_short_url?(short_url)
    return true if short_url_contains_offensive_word?(short_url)
    return true if short_url_is_all_numeric?(short_url)
    false
  end

  private

  def self.record_already_exists? short_url
    Url.find_by_short_url short_url
  end

  #We are now limiting the search space to records that start with the same letter as the generated short url
  #In sqlite, which is what I'm using in dev, the lik search is case insensitive,
  #which cuts down on the gain of limiting the scope somewhat.
  #Since the production database is undetermined at this point, and different databases handle the like differently,
  #I will leave further optimization until the production database is determined.
  def self.short_url_is_too_similar_to_an_existing_short_url? short_url
    Url.where('short_url like ?', "#{short_url.first}%").pluck(:short_url).each do |existing_url|
      count = 0
      zipped = existing_url.chars.zip short_url.chars
      zipped.each do |a, b|
        if a == b
          count += 1
        end
      end
      return true if count >= 6
    end
    false
  end

  def self.short_url_contains_offensive_word? short_url
    regex_union = Regexp.union(ShortUrlValidator.offensive_words)
    short_url.match(regex_union)
  end

  def self.short_url_is_all_numeric? short_url
    short_url =~ /^[0-9]+$/
  end

  def self.offensive_words
    ["f00" , "bar"]
  end

end
