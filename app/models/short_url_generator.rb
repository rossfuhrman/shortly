class ShortUrlGenerator

  #showing my work for the constant VALUES
  #initial_values = ['a', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 't', 'v', 'w', 'x', 'y', 'z', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'T', 'V', 'W', 'X', 'Y', 'Z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9] 
  #initial_values_minus = initial_values - ['B', 'b', 'I', 'i', 'L', 'l', 'S', 's', 'O', 'o', 'U', 'u', 'v']
  #initial_values_minus.count # => 49
  #VALUES = initial_values_minus = ['a', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 't', 'w', 'x', 'y', 'z', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'T', 'V', 'W', 'X', 'Y', 'Z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9] 
  VALUES = ['a', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 't', 'w', 'x', 'y', 'z', 'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'Q', 'R', 'T', 'V', 'W', 'X', 'Y', 'Z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9] 

  def self.generate
    url = get_a_valid_short_url
  end

  private

  def self.get_a_valid_short_url
    short_url = get_a_short_url
    while invalid?(short_url)
      short_url = get_a_short_url
    end
    short_url
  end

  def self.invalid? short_url
    ShortUrlValidator.invalid? short_url
  end

  def self.get_a_short_url
    string = (0...7).map { VALUES[rand(VALUES.length)] }.join
  end

  def self.ambiguity_lookup
    {
      'B' => '8', 
      'b' => '8',
      'I' => '1', 
      'i' => '1', 
      'L' => '1', 
      'l' => '1', 
      'S' => '5', 
      's' => '5', 
      'O' => '0', 
      'o' => '0', 
      'U' => 'V', 
      'u' => 'V',
      'v' => 'V'
    }
  end


  
end
