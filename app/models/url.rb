require 'uri'

class Url < ActiveRecord::Base
  validates_presence_of :short_url
  validates_uniqueness_of :short_url

  def self.custom_create long_url
    check_url long_url
    url = Url.new(:long_url => long_url)
    url.short_url = ShortUrlGenerator.generate
    url.save!
    url
  end

  def self.custom_find_by_id id
    url = if id =~ /^[0-9]+$/
             Url.find id.to_i
           else
             cleaned_id = remove_ambiguities id
             Url.find_by_short_url cleaned_id
           end
  end

  private

  def self.remove_ambiguities id
    characters_to_replace = ShortUrlGenerator.ambiguity_lookup.keys.join
    id.gsub(/[#{characters_to_replace}]/, ShortUrlGenerator.ambiguity_lookup)
  end

  def self.check_url long_url
    uri = URI.parse long_url
    unless (uri.kind_of? URI::HTTP) || (uri.kind_of? URI::HTTPS)
      raise URI::InvalidURIError 
    end
  end

end
